<?php
	$date = new DateTime();
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Web Site Nagumo - Tabloides Web">
    <meta name="author" content="Nagumo-TI-Desenvolvimento">
    <title>Tabloides | Nagumo</title>
    <link href="static/css/index.css?v=<?php echo $date->getTimestamp();?>" rel="stylesheet">
    <link rel="icon" href="./assets/images/nagumoIcon.png" type="icon" sizes="16x16">
    <link rel="stylesheet" href="./assets/css/global.css">
    <link rel="stylesheet " href="./assets/css/index/index.css">
</head>

<body>
    <header class="cabecalho">
        <h1>Tabloide de Ofertas</h1>
    </header>

    <main>
        <section class="container">
            <div class="botoes__tabloide">
                <a class="btn__link" href="./nagumo"><button class="btn__ofertas padrao">
                        <img class="btn__logo" src="./assets/images/logotipo-nagumo.png"
                            alt="Logo Supermercados Nagumo">
                        Ofertas Nagumo
                    </button></a>
            </div>
            <!-- <div class="botoes__tabloide">
                <a class="btn__link" href="./mixter"><button class="btn__ofertas mixter">
                        <img class="btn__logo" src="./assets/images/logo-mixter.png" alt="Logo Mixter Atacadista">
                        Ofertas Mixter
                    </button></a>
            </div> -->
        </section>
    </main>

    <footer class="rodape">
        <p>Reservado Grupo Nagumo</p>
    </footer>

</body>

</html>