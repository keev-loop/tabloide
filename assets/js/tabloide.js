const getCategoriasAPI = "http://35.168.166.249:9090/api/v1/categoria";
const getTabloideHoje = "http://35.168.166.249:9090/api/v1/tabloide/vigente?";

var url = new URL(window.location.href);
var loja = parseInt(url.searchParams.get("loja"));
var tipo = url.searchParams.get("tipo");

//> - HTML DO CARROSSEL DE INTERVALO
const carrosselIntervalo = `
    <div class="carousel-item active">
        <img class="d-block w-100" src="https://d39ytr32u863b6.cloudfront.net/assets/tabloide/intervalo.jpg" alt="Nagumo Tabloide">
    </div>
`;

// > - VERIFICA ULTIMO ACESSO
function sessao() {
  let acesso = new Date().getTime();
  let acessado = localStorage.getItem("acessado");

  if (acessado == undefined) {
    window.history.forward(1);
    localStorage.setItem("acessado", acesso);
  } else if (acesso > acessado + 10800000) {
    window.history.forward(1);
    localStorage.setItem("acessado", acesso);
  } else {
    console.log(acesso);
  }
}

//> - GERA HTML DO CARROSSEL
function GeraCarrossel(responseJson) {
  var saida = "";
  for (var i = 0; i < responseJson.length; i++) {
    if (i === 0) {
      saida =
        saida +
        `<div class="carousel-item active">
                                <img class="img-fluid d-block mx-auto" src="${responseJson[i].tabloideImagem}" alt="Nagumo Tabloide">
                            </div>`;
    } else {
      saida =
        saida +
        `<div class="carousel-item">
                                <img class="img-fluid d-block mx-auto" src="${responseJson[i].tabloideImagem}" alt="Nagumo Tabloide">
                            </div>`;
    }
  }
  return saida;
}

//> - CHAMA, RECEBE E GERA TABLOIDES
function GetTabloides() {
  fetch(getTabloideHoje + "loja=" + loja + "&tipo=" + tipo, { method: "GET" })
    .then(response => {
      return response.json();
    })
    .then(responseJson => {
      if (responseJson.length === 0) {
        $("#carrosselTabloides").html(carrosselIntervalo);
      } else {
        $("#carrosselTabloides").html(GeraCarrossel(responseJson));
      }
    })
    .catch(err => {
      $("#carrosselTabloides").html(carrosselIntervalo);
    });
}

//> - VALIDAR CATEGORIA
function ValidaCategoria(responseJson, tipo) {
  var saida = false;
  for (var i = 0; i < responseJson.length; i++) {
    if (responseJson[i].categoriaNome === tipo.toUpperCase()) {
      saida = true;
    }
  }
  return saida;
}

//> - CHAMA, RECEBE E DESTA EXISTENCIA DA CATEGORIA, SE EXISTE CHAMA
function GetCategorias() {
  fetch(getCategoriasAPI, { method: "GET" })
    .then(response => {
      return response.json();
    })
    .then(responseJson => {
      if (
        ValidaCategoria(responseJson, tipo) &&
        loja > 0 &&
        loja < 54 &&
        loja !== 5
      ) {
        GetTabloides();
      } else {
        window.location.replace("./");
      }
    })
    .catch(err => {
      $("#carrosselTabloides").html(carrosselIntervalo);
    });
}

var categorias = [
  "terca-nagumoSP",
  "terca-nagumoOsasco",
  "terca-nagumoContinental",
  "terca-nagumoRedonda",
  "feirao-nagumoSP",
  "feirao-nagumoOsasco",
  "feirao-nagumoContinental",
  "feirao-nagumoRedonda",
  "quarta-nagumoSP",
  "quarta-nagumoOsasco",
  "quarta-nagumoContinental",
  "quarta-nagumoRedonda",
  "sabado-nagumoSP",
  "sabado-nagumoOsasco",
  "sabado-nagumoContinental",
  "sabado-nagumoRedonda",
  "domingo-nagumoSP",
  "domingo-nagumoOsasco",
  "domingo-nagumoContinental",
  "domingo-nagumoRedonda",
  "terca-mixter",
  "quarta-mixter",
  "sabado-mixter",
  "domingo-mixter",
];

// for (let i = 0; i < categorias.length; i++) {
//     if (localStorage.getItem("Tabloide") == categorias[i]) {
//         let nome = categorias[i].split("-");
//         if (nome[1].substring(0, 6) == "nagumo") {
//             document.getElementById("tipoLogo").innerHTML = `<img class="logotipo__imagem" src="../../images/icones/nagumo/${nome[0]}.png" alt="Tabloide ${categorias[i]}">`;
//         } else {
//             document.getElementById("tipoLogo").innerHTML = `<img class="logotipo__imagem" src="../../images/icones/mixter/${nome[0]}.png" alt="Tabloide ${categorias[i]}">`;
//         }
//     }
// }

// > - VERIFICA ACESSO
sessao();
//> - INICIA A PRIMEIRA CHAMADA
GetCategorias();
