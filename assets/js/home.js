// verifica o dia da semana e reativa
switch (new Date().getDay()) {
  case 0:
    document
      .getElementById("btn_finalzao")
      .classList.remove("btn__link--desativado");
    document
      .getElementById("btn_domingo")
      .classList.remove("btn__link--desativado");
    break;
  case 1:
    document
      .getElementById("btn_domingo")
      .classList.remove("btn__link--desativado");
    break;
  case 2:
    document
      .getElementById("btn_terca")
      .classList.remove("btn__link--desativado");
    break;
  case 3:
    document
      .getElementById("btn_feirao")
      .classList.remove("btn__link--desativado");
    document
      .getElementById("btn_quarta")
      .classList.remove("btn__link--desativado");
    break;
  case 4:
    document
      .getElementById("btn_quarta")
      .classList.remove("btn__link--desativado");
    break;
  case 5:
    document
      .getElementById("btn_sexta")
      .classList.remove("btn__link--desativado");
    break;
  default:
    document
      .getElementById("btn_finalzao")
      .classList.remove("btn__link--desativado");
    document
      .getElementById("btn_sexta")
      .classList.remove("btn__link--desativado");
    break;
}
